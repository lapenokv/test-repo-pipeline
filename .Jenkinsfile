#!/usr/bin/env groovy

import groovy.json.JsonSlurper

def remote = [:]
remote.allowAnyHosts = true

def getPullRequestsData(jenkinsPassword) {
    process = ['bash', '-c', "curl -s -X GET -u \"${jenkinsPassword}\" \
        https://api.bitbucket.org/2.0/repositories/easyranking/\"${repository}\"/pullrequests/?pagelen=50"].execute().text;

    def jsonSlurper = new JsonSlurper();
    def object = jsonSlurper.parseText(process);
    def pullRequestsData = [:]
    for (i = 0; i < object.values.size(); i++) {
        title = object.values[i].title
        id = object.values[i].id
        sourceBranch = object.values[i].source.branch.name

        approved = ['bash', '-c', "curl -s -X GET -u \"${jenkinsPassword}\" \
            https://api.bitbucket.org/2.0/repositories/easyranking/\"${repository}\"/pullrequests/\"${id}\"/?pagelen=50"].execute().text;
        def object1 = jsonSlurper.parseText(approved)
        state = object1.participants.state

        if ( ! (state.contains('changes_requested'))){
            if (state.contains('approved')){
                pullRequestsData[title] = [id, sourceBranch]
            }
        }
    }
    return pullRequestsData
}

def getPullRequestsList(ctx) {
    def pullRequestsList = ctx.values().join('\n')
    return pullRequestsList
}

pipeline {
    agent { label 'jenkins-node-02' }
    options {
        ansiColor('xterm')
        disableConcurrentBuilds()
    }
    environment {
        JENKINS_PASSWORD = credentials('jenkinsserankingBitbucketAppPassword')
    }
        parameters {
        choice choices: ['seranking.com', 'top10seosoftware.com', 'help.seranking.com'], description: '', name: 'Production'
        choice choices: ['Merge', 'Revert'], description: '', name: 'Action'
    }
    
    stages {
        stage('Vars') {
            steps {
                script {
                if (env.Production == 'seranking.com'){
                    env.repository = "website-seranking.com"
                    env.rootdir = "seranking.com"
                    env.username = "serankingcom"
                    env.keyid = "serankingcomSSH"
                    remote.name = "seranking.com"
                    remote.host = "cloud90.sr-srv.net"
                }
                if (env.Production == 'top10seosoftware.com'){
                    env.repository = "top10seosoftware.com"
                    env.rootdir = "top10seosoftware.com"
                    env.username = "top10ss"
                    env.keyid = "serankingcomSSH"
                    remote.user = "top10ss"
                    remote.name = "top10seosoftware.com"
                    remote.host = "top10seosoftware.com"
                }
                if (env.Production == 'help.seranking.com'){
                    env.repository = "help.seranking.com"
                    env.rootdir = "help.seranking.com"
                    env.username = "helpserankingcom"
                    env.keyid = "helpserankingcomSSH"
                    remote.name = "help.seranking.com"
                    remote.host = "cloud90.sr-srv.net"
                }
                                echo "${state}"
                //echo "${approved}"
                }
            }
        }

        stage('User input') {
            when { 
                environment name: 'Action', value: 'Merge' 
            }    
            steps {
                script {
		            def PRs = getPullRequestsData(JENKINS_PASSWORD)
		 
                    def USER_CHOICES = input message: 'Select Pull Request', ok: 'Merge',
                                        parameters: [choice(choices: getPullRequestsList(PRs), name: 'PR name')] as LinkedList
        			echo "${USER_CHOICES}"
		        	env.CHANGE_ID = USER_CHOICES.split(',')[0].split('\\[')[1]
        			env.SOURCE_BRANCE = USER_CHOICES.split(',')[1].split('\\]')[0]
		        	echo "CHANGE_ID=${CHANGE_ID}, SOURCE_BRANCE=${SOURCE_BRANCE}" 
                }
            }
        }

        stage('Merge') {
            when { 
                environment name: 'Action', value: 'Merge' 
            }
            steps{
                withCredentials([
                    sshUserPrivateKey(credentialsId: 'serankingcomSSH', keyFileVariable: 'identity', passphraseVariable: '', usernameVariable: 'userName'),
                    string(credentialsId: 'jenkinsserankingBitbucketAppPassword', variable: 'APPPASS')]) {
                        sh label: '', script: 'curl -s -X POST -u "${APPPASS}" "https://api.bitbucket.org/2.0/repositories/easyranking/\"${repository}\"/pullrequests/\"${CHANGE_ID}\"/merge"'
                        echo 'Waiting 2 mins'
                        sleep 120
                }
            }
        }

        stage("pull") {
            when { 
                environment name: 'Action', value: 'Merge' 
            }
            steps {
                script {
                    remote.allowAnyHosts = true
                    withCredentials([
                        sshUserPrivateKey(credentialsId: "${keyid}", keyFileVariable: 'identity', passphraseVariable: '', usernameVariable: "${username}"),
                        string(credentialsId: 'jenkinsserankingBitbucketAppPassword', variable: 'APPPASS')
                            ]) {
                        remote.user = userName
                        remote.identityFile = identity
        
                        writeFile file: 'script.sh', text: "cd ~/${rootdir}\ngit checkout master\ngit pull > ~/gitpull.log 2>&1"
                        sshPut remote: remote, from: 'script.sh', into: '.'
                        sshScript remote: remote, script: "script.sh"
                        sshRemove remote: remote, path: "script.sh"

                        slackSend channel: '#aguzov', message: "${rootdir} PR ${CHANGE_ID} ${SOURCE_BRANCE} merged to master", teamDomain: 'seranking', tokenCredentialId: 'SlackAguzov'
                    }
                }
            }
        }


        stage('Revert') {
            when { 
                environment name: 'Action', value: 'Revert' 
            }
            steps{
                script{
                    remote.allowAnyHosts = true
                    
                    def NumberPRs = input message: 'Insert number of revert PRs', ok: 'Revert',
                        parameters: [string(name: 'NUMBER', defaultValue: '1', description: '')]

                    withCredentials([
                        sshUserPrivateKey(credentialsId: "${keyid}", keyFileVariable: 'identity', passphraseVariable: '', usernameVariable: "${username}"),
                        string(credentialsId: 'jenkinsserankingBitbucketAppPassword', variable: 'APPPASS')
                        ]) {
                        remote.user = userName
                        remote.identityFile = identity

                        writeFile file: 'script.sh', text: "cd ~/${rootdir}\ngit checkout master\ngit checkout HEAD@~${NumberPRs} > ~/gitrevert.log 2>&1"
                        sshPut remote: remote, from: 'script.sh', into: '.'
                        sshScript remote: remote, script: "script.sh"
                        def commandResult = sshCommand remote: remote, command: "grep HEAD ~/gitrevert.log"
                        sshRemove remote: remote, path: "script.sh"

                        slackSend channel: '#aguzov', message: "${rootdir} last PR reverted. ${commandResult}", teamDomain: 'seranking', tokenCredentialId: 'SlackAguzov'
                    }
                }
            }
        }

        stage('Check PROD') {
		steps{
                	script{
				PRODSTATE = ['bash', '-c', "curl -s -L -m 15 -X GET https://\"${rootdir}\" | grep -o \"${rootdir}\" | wc -l"].execute().text.toInteger();
				echo "PROD == ${PRODSTATE} if > 3, than OK"
				if ( PRODSTATE < 50 ) {
					echo "!!!!ALERT SOMETHING WAS WRONG!!!! CHECK ${rootdir} PROD == ${PRODSTATE}"
					slackSend channel: '#aguzov', message: "${rootdir} ALERT SOMETHING WAS WRONG. Chekc prod.", teamDomain: 'seranking', tokenCredentialId: 'SlackAguzov'
				}
				if ( env.Production == 'seranking.com' ) {
					PRODSTATE = ['bash', '-c', "curl -s -L -m 15 -X GET https://\"${rootdir}\"/blog/ | grep -o \"${rootdir}\" | wc -l"].execute().text.toInteger();
					if ( PRODSTATE < 50 ) {
						echo "!!!!ALERT SOMETHING WAS WRONG!!!! CHECK ${rootdir}/blog !!!!! PROD/blog == ${PRODSTATE}"
					}
				} 
			}
		}
        }
    }
}